Proyek ini memecahkan permasalahan Propensi Group Clustering. Proyek ini dikerjakan oleh kelompok Elba, yang beranggotakan 3 orang (semoga 3 beneran):

- Muhammad Anwar Farihin - 1706039635
- Muhammad Irfan Amrullah - 1706039585
- Muhammad Nadhirsyah Indra - 1706039383

Input:
Setiap mahasiswa, memasukkan parameter-parameter berikut:

- Urutan prioritas 1-6 (Role ada 6: Project Manager, Lead System Analyst, Lead Programmer, Lead System Designer, Lead Tester, Lead Technical Writer).
- 1 mahasiswa yg ingin sekelompok.
- 1 mahasiswa yg tidak ingin sekelompok.

Output:

- Setiap Mahasiswa mendapatkan kelompok terbaiknya yang paling sesuai dengan parameter yang dimasukkan diawal

contoh input (gambaran kasar):

```
% fakta-fakta (diberikan sekitar 30-60 fakta)
mahasiswa (khalis, role(PM,LP,LSD,...), irfan, nadhir) .
mahasiswa (nadhir, role(LT,LP,LSD,...), khalis, farihin) .
% ....
```

```
% rules
```

contoh output (gambaran kasar):

```
% output
kelompok_akhir = [[khalis, dhiba, falkro , ..],
                [nadhir, irfan, farihin, ..],
                [fadhlan, jere, rae, ..],
                ]
```
