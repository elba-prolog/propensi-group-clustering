:- dynamic rolePrioritas/3.
:- dynamic isGroup/1.
:- dynamic hateRelation/2.
:- dynamic loveRelation/2.

% available role 
role(pm).
role(lsa).
role(lp).
role(lsd).
role(lt).
role(ltw).

% initiate mahasiswa as input
makemahasiswa:-
    mahasiswa(farihin, [lt, pm, lsa, lsd, lp, ltw], nadhir, dhafin),
	mahasiswa(irfan, [lsd, ltw, lt], nadhir, dhafin),
	mahasiswa(nadhir, [lsa, pm, lsd, ltw, lp, lt], khalis, bayu),
 	mahasiswa(khalis, [lp, pm, ltw, lt, lsd, lsa], ray, sayid),
	mahasiswa(ray, [lt, lsa, lsd, ltw, pm, lp], bayu, irfan),
	mahasiswa(nata, [lsa, lt, ltw, lp, lsd, pm], ray, farihin),
    mahasiswa(bayu, [lsa, lt, lsd, lp, pm, ltw], dhafin, nadhir),
	mahasiswa(sayid, [lp, lsd, ltw, lsa, lt, pm], dhafin, farihin),
	mahasiswa(dhafin, [ltw, lt, lp, pm, lsa, lsd], nadhir, ray),
    mahasiswa(aryo, [lt, lsa, lsd, pm, lp, ltw], jere, nio),
    mahasiswa(ami, [lsd, ltw,lsa, pm, lp, lt], jere, nio),
    mahasiswa(jere, [lt, lsd, lsa, lp, pm, ltw], fadhlan, jeje),
    mahasiswa(fadhlan, [ltw, pm, lt, lsa, lsd, lp], adip, ajay),
    mahasiswa(adip, [lsa, lsd, pm, lt, ltw, lp], jeje, ami),
    mahasiswa(akbar, [lt, ltw, lsd, pm, lsa, lp], adip, aryo),
    mahasiswa(jeje, [lsa, lt, pm, ltw, lsd, lp], nio, jere),
    mahasiswa(ajay, [ltw, lsa, lt, pm, lp, lsd], nio, aryo),
    mahasiswa(nio, [lp, pm, lsa, ltw, lt, lsd], jere, adip).

main(Result):-
    makemahasiswa,
    bagof(rolePrioritas(Person,Role,Index), rolePrioritas(Person,Role,Index), List),
    makeagroup(List, [], [], [],[]),
    bagof(List1, isGroup(List1), Result),
    !.

% create group based constraint given
% base case
makeagroup([],[],_, _,_).

% if all mahasiswa already iterared but there is group that is not saved
makeagroup([],TotalOutput,_, _,_):-
    findall(Role,role(Role),AllRole),
    length(TotalOutput,LengthOutput),
    length(AllRole,LengthAllRole),
    LengthOutput == LengthAllRole,
    !,
    assert(isGroup(TotalOutput)).

% if there is group that want to be saved but there are still mahasiswa who have not iterated
makeagroup([_|Tail], Output, InsertedPerson, _,_):-
    findall(Role,role(Role),AllRole),
    length(Output,LengthOutput),
    length(AllRole,LengthAllRole),
    LengthOutput == LengthAllRole,
    !,
    assert(isGroup(Output)),
    makeagroup(Tail, [], InsertedPerson, [],[]).

% if mahasiswa already in saved group
makeagroup([rolePrioritas(P,_,_)|Tail], Output, InsertedPerson, InsertedRole, TempGroup):-
    member(P, InsertedPerson),
    !,
    makeagroup(Tail, Output, InsertedPerson, InsertedRole, TempGroup).

% if the role that mahasiswa wants already in group 
makeagroup([rolePrioritas(_,R,_)|Tail], Output, InsertedPerson, InsertedRole, TempGroup):-
    member(R, InsertedRole),
    !,
    makeagroup(Tail, Output, InsertedPerson, InsertedRole,TempGroup).

% saved mahasiswa to group
makeagroup([rolePrioritas(P,R,I)|Tail], Output, InsertedPerson, InsertedRole, TempGroup):-
    setof(Love,loveRelation(P,Love), ListLove),
    getFilteredPerson(ListLove, [], FilteredLovePerson),
    subtract(Tail,FilteredLovePerson,TempLoveSubsList),
    append(FilteredLovePerson, TempLoveSubsList, TempLoveAddList),
    
    setof(Hate,hateRelation(P,Hate), ListHate),
    getFilteredPerson(ListHate, [], FilteredHatePerson),
    subtract(TempLoveAddList,FilteredHatePerson,TempHateSubsList),
    append(TempHateSubsList, FilteredHatePerson, TempHateAddList),
    
    append([P], InsertedPerson, InsertedPerson1),
    append([P], TempGroup, TempGroup1),
    append([R], InsertedRole, InsertedRole1),
    append([rolePrioritas(P,R,I)], Output, Output1),
    makeagroup(TempHateAddList, Output1, InsertedPerson1, InsertedRole1,TempGroup1).

% assign hate and love relation
mahasiswa(Name, [Head|Tail], Hate, Love):-
    assert( hateRelation(Name,Hate) ),
    assert( hateRelation(Hate,Name) ),
    assert( loveRelation(Love,Name) ),
    assert( loveRelation(Name,Love) ),
    mahasiswa(Name, [Head|Tail], Hate, Love, 0).

% base case for saving relation
mahasiswa(_,[],_,_,_).

% assign role relation
mahasiswa(Name, [Head|Tail], Hate, Love, Index):-
    assert( rolePrioritas(Name,Head,Index) ),
    Index1 is Index + 1,
    mahasiswa(Name, Tail,Hate, Love,Index1).

% get role relation from spesific mahasiswa
getFilteredPerson([],TotalFilteredPerson,TotalFilteredPerson).
getFilteredPerson([Head|Tail], FilteredPerson, TotalFilteredPerson):-
    setof(rolePrioritas(Head,Role,Index),rolePrioritas(Head,Role,Index), TempFilteredPerson),
    append(TempFilteredPerson, FilteredPerson, FilteredPerson1),
    getFilteredPerson(Tail, FilteredPerson1, TotalFilteredPerson).